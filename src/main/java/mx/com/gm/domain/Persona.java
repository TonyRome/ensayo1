/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gm.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author tony
 */

@Entity
@Table (name = "persona")  //Se agrega esta parte para definir nombre de la tabla DDBB, el problema era quela query tomaba tabla como test.PERSONA y no test.persona
@NamedQueries({
    @NamedQuery (name = "Persona.encontrarTodasPersonas", query = "SELECT p FROM Persona p ORDER BY p.idPersona")
})
public class Persona implements Serializable {
    
    //Se especifica la llave primaria
    @Id
    //Como se genera la llave primari
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    //Se indica elnombre en base de datos, solo se especifica si  los nombres DDBB y JAVA son diferentes
    @Column(name = "id_persona")
    
    private int idPersona;
    private String nombre;
    
    //Constructor vacio
    public Persona(){}
    
    //Constructor para recibir llave primaria
    public Persona (int idPersona){
        this.idPersona = idPersona;
    }
    
    //Constructor que recibe todo los argumentos
    public Persona (int idPersona, String nombre){
        this.idPersona = idPersona;
        this.nombre = nombre;
    }

    //Se genera los atributos
    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    //funcion to String
    @Override
    public String toString() {
        return "Persona{" + "idPersona=" + idPersona + ", nombre=" + nombre + '}';
    }
    
}
