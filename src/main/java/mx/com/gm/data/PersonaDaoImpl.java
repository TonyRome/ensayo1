/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gm.data;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.*;
import mx.com.gm.domain.Persona;

/**
 *
 * @author tony
 */
//Para convertirlo en ejb y ocupar persistencia
@Stateless
public class PersonaDaoImpl implements PersonaDao{

    @PersistenceContext (unitName = "PersonaPU")
    EntityManager em;
    
    @Override
    public List<Persona> encontrarTodasPersonas() {
        //Llama la query edifinada en clase Persona
        return em.createNamedQuery("Persona.encontrarTodasPersonas").getResultList();
    }

    @Override
    public Persona encontrarPersona(Persona persona) {
        //Busca objeto tipo persona con el ID
        return em.find(Persona.class, persona.getIdPersona());
    }

    @Override
    public void insertarPersona(Persona persona) {
        em.persist(persona);
        //Para regresar llave primaria de persona de forma automatica
        em.flush();
    }

    @Override
    public void actualizarPersona(Persona persona) {
        em.merge(persona);
    }

    @Override
    public void eliminarPersona(Persona persona) {
        //Actualiza estado de DDBB por merge, despues se llama metodo remove, caso contrario puede dar error
        em.remove(em.merge(persona));
    }
    
}
