/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gm.servicio;

import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.Status;
import mx.com.gm.data.PersonaDao;
import mx.com.gm.domain.Persona;

/**
 *
 * @author tony
 */
@Stateless
@Path("/personas")
public class PersonaServiceRS {
    
    @Inject
    private PersonaDao personaDao;
    
    //Obtiene informacion
    @GET
    //Que tipo de informacion produce, JSON
    @Produces (value = MediaType.APPLICATION_JSON)
    //METODO REST
    public List<Persona> listarPersonas(){
        List<Persona> personas = personaDao.encontrarTodasPersonas();
        System.out.println("Personas encontradas: " + personas);
        return personas;
    }
    
    //Obtiene informacion
    @GET
    //Produce informacion JSON
    @Produces (value = MediaType.APPLICATION_JSON)
    //Se recibe un parametro
    @Path("{id}") //Hace referencia al path: /personas/{id} , ejemplo /personas/1
    public Persona encontrarPersona(@PathParam("id") int id){
        Persona persona = personaDao.encontrarPersona(new Persona(id)); //Crea una persona con el id , lo pasa al constructor pra buscarlo
        System.out.println("Persona encontrada: " + persona);
        return persona;
    }
    
    @POST
    //Consume informacion
    @Consumes (value = MediaType.APPLICATION_JSON)
    //Produce informacion tambien
    @Produces (value = MediaType.APPLICATION_JSON)
    public Persona agregarPersona(Persona persona){
        personaDao.insertarPersona(persona);
        System.out.println("Persona agregada: " + persona);
        return persona;
    }
    
    //Actualiza
    @PUT
    @Consumes (value = MediaType.APPLICATION_JSON)
    @Produces (value = MediaType.APPLICATION_JSON)
    @Path("{id}")
    public  Response modificarPersona(@PathParam("id") int id, Persona personaModificada){
        Persona persona = personaDao.encontrarPersona(new Persona(id));
        if(persona != null){
            personaDao.actualizarPersona(personaModificada);
            System.out.println("Persona modificada: " + personaModificada);
            return Response.ok().entity(personaModificada).build();
        }
        else{
            return Response.status(Status.NOT_FOUND).build();
        }
    }
    
    @DELETE
    @Produces (value = MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Response eliminarPersona(@PathParam("id") int id){
        personaDao.eliminarPersona(new Persona(id)); //Convierte el id en persona
        System.out.println("Persona eliminada: " + id);
        return Response.ok().build();
    }
    

}